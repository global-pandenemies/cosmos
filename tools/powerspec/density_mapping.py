import pandas as pd
import numpy as np
import math

data_dir = '../../data/'

regions = {
    'us': {'info':data_dir+'misc/us/info.csv','scale_long':92},
    'uk': {'info':data_dir+'misc/uk/info.csv','scale_long':65},
} 

def assign_nodes_to_grid(nodes,info_data,N,scale_long):
    
    grid = np.ndarray(shape=(N,N), dtype=float)
    # 1 deg Long ~ 92 km (@35 deg N/S), 1 deg Lat = 111.3 km
    scale_lat = 111.3
    scale = max(scale_long*abs(max(info_data['Long'])-min(info_data['Long'])),scale_lat*abs(max(info_data['Lat'])-min(info_data['Lat'])))/N
    offset_long = -min(info_data['Long'])
    offset_lat = -min(info_data['Lat'])

    for uid in nodes:
        area = info_data.loc[uid,'Area']/scale/scale
        size = math.sqrt(area)
        center = [(info_data.loc[uid,'Long']+offset_long)*scale_long,(info_data.loc[uid,'Lat']+offset_lat)*scale_lat]
        mass = info_data.loc[uid,'Population']

        for x in range(math.floor(center[0]-size/2),math.ceil(center[0]+size/2)):
            for y in range(math.floor(center[1]-size/2),math.ceil(center[1]+size/2)):
                
                w = (abs(center[0]-x)-size/2) if abs(center[0]-x) < size/2  else 1
                h = (abs(center[1]-y)-size/2) if abs(center[1]-y) < size/2  else 1

                grid[x][y] += mass*w*h/area


#info_data = pd.read_csv(info,header=0,dtype={'UID':str})
