import pandas as pd
import numpy as np
from scipy.optimize import minimize, Bounds
from matplotlib import pyplot as plt
import os

os. chdir('c:/tmp')
dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y')
#case_data = pd.read_table('coronavirus-cases.csv', sep=',', parse_dates=['Specimen date'], date_parser=dateparse)
case_data = pd.read_excel('UKFlowModelData.xlsx', sheet_name='Cases')
np.min(case_data['Specimen date'].tolist())
np.max(case_data['Specimen date'].tolist())
dates = [d for d in set(case_data['Specimen date'].tolist())]

case_data = case_data[case_data['Area type'] == 'Upper tier local authority']
piv = pd.pivot_table(
    case_data,
    values='Daily lab-confirmed cases',
    index=['Specimen date'],
    columns='Area code'
)

areas = piv.columns.to_list()
M=len(areas)

utla_to_ltla = pd.read_excel('UKFlowModelData.xlsx', sheet_name='UT to LT Lookup')
flow_data = pd.read_excel('UKFlowModelData.xlsx', sheet_name='FlowWithPop')
population = pd.read_excel('UKFlowModelData.xlsx', sheet_name='PopulationDataByAge')
lat_long = pd.read_excel('UKFlowModelData.xlsx', sheet_name='LatLongs')

flow_data1 = pd.merge(flow_data, utla_to_ltla, how='left', left_on='OriginCode', right_on='LTLA19CD', indicator='1')
flow_data1.rename(columns={'UTLA19CD':'origin_utla'},inplace=True)
flow_data1 = pd.merge(flow_data1, utla_to_ltla, how='left', left_on='DestinationCode', right_on='LTLA19CD')
flow_data1.rename(columns={'UTLA19CD':'dest_utla'},inplace=True)
population1 = pd.merge(population, utla_to_ltla, how='left', left_on='Code',right_on='LTLA19CD')
lat_long1 = pd.merge(lat_long, utla_to_ltla, how='left', left_on='lad17cd',right_on='LTLA19CD')

# raw flows
# another piss poor join!
#flow_data[(flow_data.OriginLabel=='Ealing') &   (flow_data['All categories: Method of travel to work;']>5000)]['DestinationLabel']
F = np.zeros((M,M))
for i in range(0,M):
    print(i/M)
    for j in range(0,M):
        try:
            f = flow_data[(flow_data1.origin_utla==areas[i]) & (flow_data1.dest_utla==areas[j])]['All categories: Method of travel to work;'].sum()
            #print(e)
            F[i,j] = f
        except:
            False
            #print('a')

# population
P=np.zeros((M,))
for i in range(0,M):
    P[i] = population[population1.UTLA19CD==areas[i]]['All usual residents'].sum()
P[P==0]=100000 # todo:sort this out
P/=np.sum(P)

# ffs how do i do this join properly?
x = np.zeros((M,))
y = np.zeros((M,))
for i in range(0,len(areas)):
    x[i] = lat_long1[(lat_long1.UTLA19CD==areas[i])]['long'].mean()
    y[i] = lat_long1[(lat_long1.UTLA19CD==areas[i])]['lat'].mean()
# a few unmapped ones
x[np.isnan(x)]=np.mean(x[~np.isnan(x)])
y[np.isnan(y)]=np.mean(y[~np.isnan(y)])

if False:
    #destroy the geo information, but preserving self-distnace=0
    np.random.shuffle(x)
    np.random.shuffle(y)

# distance
D =( (x.reshape((M,1)) - x.reshape((1,M)))**2 + (y.reshape((M,1)) - y.reshape((1,M)))**2)**.5
D/=np.max(D)

if False:
    # completely jumble the distnances
    np.random.shuffle(D)

X = np.array(piv.fillna(0.0))

mu = 0.01       # rate of imported cases, per area per day
alpha = 0.3   # self-exciting rate param
beta = 0.2     # time decay of self-excitement
gamma = 3
w2 = 0.25
w3 = 0.25
w4 = 0.25

base_params = (mu, alpha, beta, gamma, w2, w3, w4)

M = X.shape[1]
R = alpha * M / beta


def neg_log_likelihood(params, X):
    # approximating in discrete time
    mu, alpha, beta, gamma, w2, w3, w4 = params
    T = X.shape[0]
    M = X.shape[1]
    base_intensity = mu * np.ones_like(X)
    #A = alpha * np.ones((M, M))/M  # matrix describing transport of cases
    #A1 = np.ones((M, M))/M
    A1 = np.tile(P.T,(M,1)) # anywhere, proportional to population
    A2 = np.eye(M)  # local transmission
    A3 = np.tile(P.T,(M,1)) * np.exp(-gamma*D) # population and distance
    A3 /= np.sum(A3, axis=1).reshape((M, 1))
    A4 = F + F.T # 2 way commuter flow
    A4 /= (1+np.sum(A4, axis=1).reshape((M, 1)))

    # todo: handle more than two contributions and the relevant contraint
    #A = alpha * ((1-w)*A1 + w*A4)  # matrix describing transport of cases
    A = alpha * (np.max((0,(1 - w2 -w3 -w4))) * A1 + w2 * A2 + w3*A3 +w4 * A4)  # matrix describing transport of cases
    se_intensity = np.zeros_like(X)
    for t in range(1, T):
        se_intensity[t, :] = np.exp(-beta) * se_intensity[t - 1, :] + A @ X[t - 1, :]
    intensity = base_intensity + se_intensity
    # log likelihood is just sum of log intensity at arrival times less integrated hazard rate
    ll = -np.sum(np.sum(X * np.log(intensity) - intensity, axis=1), axis=0)
    return ll


bounds = Bounds([0.001, 0.05, 1 / 20, 0, 0, 0, 0], [0.1, 0.5, 1 / 2, 300, 1, 1, 1])
best_min = minimize(
    neg_log_likelihood,
    base_params,
    args=(X[:-10,:],),
    bounds=bounds
)

B = np.array([[0,0,0],[1,0,0],[1,.5,0],[1,.5,.5]])
for k in range(0,B.shape[0]):
    bounds = Bounds([0.001, 0.05, 1 / 20, 0, 0, 0, 0], [0.1, 0.5, 1 / 2, 300, B[k,0],B[k,1],B[k,2]])
    best_min = minimize(
        neg_log_likelihood,
        base_params,
        args=(X[:-10, :],),
        bounds=bounds
    )

    w2 = best_min.x[-3]
    w3 = best_min.x[-2]
    w4 = best_min.x[-1]
    print('Model' + str(k) + ' : ' + 'Everywhere ' + str(1-w2-w3-w4) +
          (' Local ' + str(w2) if B[k,0]>0 else '') +
          (' Distance ' + str(w3)  if B[k,1]>0 else '')+
          (' Commuter flow ' + str(w4) if B[k,2]>0 else '')+
          ' Likelihood ' + str(-best_min.fun))


gamma=5
A1 = np.tile(P.T,(M,1)) # anywhere, proportional to population
A2 = np.eye(M)  # local transmission
A3 = np.tile(P.T,(M,1)) * np.exp(-gamma*D) # population and distance
A3 /= np.sum(A3, axis=1).reshape((M, 1))
A4 = F + F.T # 2 way commuter flow
A4 /= (1+np.sum(A4, axis=1).reshape((M, 1)))
fig, axs = plt.subplots(2,2)
axs[0][0].imshow(A1)
axs[0][1].imshow(A2)
axs[1][0].imshow(A3)
axs[1][1].imshow(A4)
axs[0][0].set_title('Everywhere')
axs[0][1].set_title('Local')
axs[1][0].set_title('Distance')
axs[1][1].set_title('Commuter flow')
plt.savefig('C:/coronahack/cosmos/tools/models/spatial_hawkes/spatial_structure')


#

mu, alpha, beta, gamma, w2, w3, w4 = best_min.x
T = X.shape[0]
M = X.shape[1]
base_intensity = mu * np.ones_like(X)
A = alpha * ((1 - w2 - w3 - w4) * A1 + w2 * A2 + w3 * A3 + w4 * A4)  # matrix describing transport of cases
se_intensity = np.zeros_like(X)

se_intensity1 = np.zeros_like(X)
se_intensity2 = np.zeros_like(X)
se_intensity3 = np.zeros_like(X)
se_intensity4 = np.zeros_like(X)

for t in range(1, T):
    se_intensity[t, :] = np.exp(-beta) * se_intensity[t - 1, :] + A @ X[t - 1, :]
    se_intensity1[t, :] = np.exp(-beta) * se_intensity1[t - 1, :] + alpha * (1-w2-w3-w4) * A1 @ X[t - 1, :]
    se_intensity2[t, :] = np.exp(-beta) * se_intensity2[t - 1, :] + alpha *w2 * A2 @ X[t - 1, :]
    se_intensity3[t, :] = np.exp(-beta) * se_intensity3[t - 1, :] + alpha *w3 * A3 @ X[t - 1, :]
    se_intensity4[t, :] = np.exp(-beta) * se_intensity4[t - 1, :] + alpha *w4 * A4 @ X[t - 1, :]

intensity = base_intensity + se_intensity

j = 99 # Ealing
plt.figure()
plt.plot(X[:,j])
plt.plot(base_intensity[:,j])
plt.plot(se_intensity[:,j])
plt.plot(se_intensity1[:,j])
plt.plot(se_intensity2[:,j])
plt.plot(se_intensity3[:,j])
plt.plot(se_intensity4[:,j])
plt.legend(['Actual','E(Imported)','E(UK)','E(UK - everywhere)','E(UK -local)','E(UK - distance)','E(UK commuter flow)'])
plt.title('UK cases to Apr 6 - Ealing')
plt.savefig('C:/coronahack/cosmos/tools/models/spatial_hawkes/example_timeseries')

h=open('C:/coronahack/cosmos/tools/models/spatial_hawkes/timeline.pkl','wb')
pickle.dump({'intensity':intensity,'area':areas,'dates':dates},h)



# alphas = np.linspace(0.0005, 0.003, 10)  # alphas designed to give sensible Rs from betas under consideration
# betas = np.linspace(1 / 20, 1 / 5, 12)  # char time ranging from 5 days to 20 days

# ll=np.zeros((10, 12))
# for i in range(0, len(alphas)):
#     for j in range(0, len(betas)):
#         params = base_params
#         params['alpha'] = alphas[i]
#         params['beta'] = betas[j]
#         ll[i, j] = log_likelikhood(params, X[:-10, :])
