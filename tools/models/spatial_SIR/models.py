import pandas as pd
import csv
import numpy as np
from collections import OrderedDict
from datetime import datetime
import copy

def discrete_SIR(N, S, I, beta, gamma, beta_work=None, flow=None):

    if beta_work is None:
        beta = beta/2
        beta_work = beta
    
    if flow is not None:
        flow = flow.copy()  # to avoid changing original flow matrix
        np.fill_diagonal(flow, 0)  # set diagonal of flow to zero
        
        IoverN_work = (I * (1. - np.sum(flow, axis=1)) + np.dot(I, flow)) /  (N * (1. - np.sum(flow, axis=1)) + np.dot(N, flow))  # notice: np.dot(I,C) == np.dot(C^T, I)
        dS = -S * ( beta * I / N + beta_work * np.dot(flow, IoverN_work) + beta_work * IoverN_work * (1 - np.sum(flow, axis=1)))
    else:  # assume no flows i.e. flow = diag(1)
        dS = -(beta+beta_work) * S * I / N

    dI = -dS - gamma * I
    
    return {'dS': dS, 'dI': dI}


def discrete_SEIR(N, S, I, beta, sigma, gamma, beta_work=None, flow_work=None):

    if beta_work is None:
        beta = beta/2
        beta_work = beta
    
    if flow is not None:
        flow = flow.copy()  # to avoid changing original flow matrix
        np.fill_diagonal(flow, 0)  # set diagonal of flow to zero
        
        IoverN_work = (I - I * np.sum(flow, axis=1) + np.dot(I, flow)) /  (N - N * np.sum(flow, axis=1) + np.dot(N, flow))  # notice: np.dot(I,C) == np.dot(C^T, I)
        dS = -( S * ( beta * I / N + beta_work * np.dot(flow, IoverN_work)) + beta_work * IoverN_work * (1 - np.dot(flow, S)))
    else:  # assume no flows i.e. flow = diag(1)
        dS = -(beta+beta_work) * S * I / N

    dE = -dS - sigma * E
    dI = -dE - gamma * I

    return {'dS': dS, 'dE': dE, 'dI': dI}


available_models = {'SIR' : discrete_SIR, 'SEIR' : discrete_SEIR}


class Simulation:

    history = None
    config = {}
    
    def __init__(self, model, config, topology=None, mobility=1., keep_history=True, extrapolate_mobility=True):

        if model not in available_models:
            raise Exception('Model not available/known:', model)
        self.model = available_models[model]
        # config contains state of sim i.e. step number, parameters, populations, etc 
        self.config = copy.deepcopy(config)
        self.step = 0
        
        self.topology = topology
        self.mobility = mobility
        if keep_history:
            self.history = pd.DataFrame()
            self.history = self.history.append({'step': self.step, **copy.deepcopy(self.config)}, ignore_index=True)
            
    def run(self, nSteps=1):
        
        for i in range(nSteps):
            # calculate flow matrix for this step (i.e. mobility * topology), if mobility is 2-dim array, get vector for step 
            flow = None
            if self.topology is not None:
                if np.isscalar(self.mobility) or self.mobility.ndim < 2:
                    flow = self.mobility * self.topology
                else:
                    flow = self.mobility[:, min(self.step, self.mobility.shape[1] - 1)] * self.topology

            # call model with parameters stored in config
            diffs = self.model(**self.config, flow=flow)
            # use dictionary of changes to parameters returned from model to update existing config for next step where necessary
            for k in set(diffs):
                self.config[k[1:]] += diffs[k]

            self.step += 1
                
            if self.history is not None:
                self.history = self.history.append({'step': self.step, **copy.deepcopy(self.config), **diffs}, ignore_index=True)

        if not np.isscalar(self.mobility) and self.mobility.ndim == 2 and self.mobility.shape[1] <= self.step:
            print("Notice:",self.step - self.mobility.shape[1] + 1, "steps used extrapolated mobility data") 
            
        return self.config

    
def import_cases_file(cases_file):

    cases_df = pd.read_csv(cases_file, parse_dates=[2], dtype={'UID':str})

    # get starting date of sim
    date = cases_df.loc[0]['date'].date()

    nodes = OrderedDict()
    pops = {'N': [], 'dI': [], 'I_cumm': []}

    for index, row in cases_df.iterrows():

        nodes[row['UID']] = index

        pops['N'].append(row['Population'])
        pops['dI'].append(row['Confirmed_daily'])
        pops['I_cumm'].append(row['Confirmed']) 

    pops['N'] = np.array(pops['N'])
    pops['dI'] = np.array(pops['dI'])
    pops['I_cumm'] = np.array(pops['I_cumm'])
        
    return nodes, date, pops
    

def import_mobility_file(mob_file, nodes, start_date):

    mob_df = pd.read_csv(mob_file, parse_dates=[3], dtype={'UID':str})
    n_days = 0
    for index, row in mob_df.iterrows():
        if (row['date'].date() - start_date).days > n_days:
            n_days = (row['date'].date() - start_date).days
    
            mobility = np.empty(shape=(len(nodes), n_days+1))
    mobility[:] = np.NaN

    for index, row in mob_df.iterrows():
        date_index = (row['date'].date() - start_date).days
        mobility[nodes[row['UID']], date_index] = 1. + row['mobility_change']

    # sanity check: have all nodes had a mobility defined for each day (technically already guaranteed)
    assert(not np.isnan(np.sum(mobility)))

    return mobility


def import_topology_file(top_file, nodes):

    # create empty topology
    topology = np.array(np.zeros(shape=(len(nodes),len(nodes))))
                         
    # now populate topology with weights from file
    top_df = pd.read_csv(top_file,dtype={'UID_work':str,'UID_residence':str})
    for index, row in top_df.iterrows():
        topology[nodes[row['UID_residence']], nodes[row['UID_work']]] = row['norm_flow']
                                    
    return topology

    
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Run sim of a specified model')
    parser.add_argument('-m', '--model', help="specifies model (available: SIS,SEIR)", required=True)
    parser.add_argument('-p', '--params', help="specifies model parameters as comma-seperated list", required=False)
    parser.add_argument('-c', '--population_file', help="specifies ini file for populations", required=True)
    mobility_group = parser.add_mutually_exclusive_group()
    mobility_group.add_argument('-r', '--mobility_file', help="specifies ini file/constant factor for dynamic mobility (optional)", required=False)
    mobility_group.add_argument('-R', '--mobility_rate', help="specifies constant factor for mobility (optional, default 1)", required=False)
    parser.add_argument('-t', '--topology', help="specifies ini file for topology (weights between nodes for cross-infection in spatial evolution) (optional)", required=False)
    parser.add_argument('-n', '--nsteps', help="specifies number of steps run", required=True)
    parser.add_argument('-q', '--testing_quality', help="specifies how many percentage of new cases detected by testing (optional, default 1)", required=False)
    parser.add_argument('-o', '--outfile', help="output file", required=True)

    args = parser.parse_args()
           
    model = args.model
    params = args.params
    nSteps = args.nsteps

    test_quality = args.get('testing_quality', 1.)
    
    config = {}

    pops, start_date, nodes = import_cases_file(args.population_file)
           
    if model == 'SIR':
        config['beta'], config['gamma'] = params.split(",")
        config['beta'] = float(config['beta'])
        config['gamma'] = float(config['gamma'])
        config['N'] = pops['N']
        config['S'] = config['N'] - pops['I_cumm'] / test_quality
        config['I'] = pops['I_cumm'] / test_quality  # TODO assume all cases still active, better way to get it would be to use a rolling sum over dI for last 1/gamma days
           
    elif model == 'SEIR':
        config['beta'], config['sigma'], config['gamma'] = params.split(",")
        config['beta'] = float(config['beta'])
        config['gamma'] = float(config['sigma'])
        config['gamma'] = float(config['gamma'])

        config['N'] = pops['N']
        config['S'] = pops['N'] - pops['I_cumm'] / test_quality  - config['E']
        config['I'] = pops['I_cumm'] / test_quality  # TODO assume all cases still active, better way to get it would be to use a rolling sum over dI for last 1/gamma days
        config['E'] = config['I']  # assuming that the number of exposed is about the same as infectionous
           
    mobility = args.get('mobility_rate', 1.)
    if 'mobility_file' in args:
        mobilty = import_mobility_file(args.mobility_file, nodes, start_date)

    topology = None
    if 'topology' in args:
        mobilty = import_topology_file(args.topology_file, nodes)
    
    sim = Simulation(model, config, topology, mobility)
    sim.run(nSteps)

    # convert history into pandas dataframe and export

#    result = pd.DataFrame()
#    for i, row in sim.history.iterrows():
        
#        date = start_date + datetime.timedelta(days=row['step'])
#            for node, index in nodes:
                
        
