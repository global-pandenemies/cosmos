import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import argparse

data_dir = '../../data/'

regions = {
    'us': {'hierarchy':data_dir+'misc/us/hierarchy.csv',
           'info':data_dir+'misc/us/info.csv',
           'flow':data_dir+'flow/us/flow_graph.csv',
           'cases':data_dir+'covid/us/timeline.csv',
           'mobility':data_dir+'mobility/us/community_mobility.csv',
       },
    'uk': {'hierarchy':data_dir+'misc/uk/hierarchy.csv',
           'info':data_dir+'misc/uk/info.csv',
           'flow':data_dir+'flow/uk/flow_graph.csv',
           'cases':data_dir+'covid/uk/timeline.csv',
           'mobility':data_dir+'mobility/uk/community_mobility.csv',
       },
}  

# we assume that we have cleaned, standardized sources for this (see data folder)

# this determines the resolution based on available data (lowest res for flow and case data at time of initial conditions) and returns a set of uids for the entities

def determine_least_common_res(case_data,flow_data,hierarchy):

    # first expand the hierarchy trees for both i.e. if we find a node we also add all the siblings (as we assume that the data is implicitly complete on that resolution and that parents exist)
    # we also expect that flow data res for work and residence is identical (so we can simply take the union of both)

    # add all siblings for each node/leaf

    case_uids=set(hierarchy[hierarchy['UID_parent'].isin(list(hierarchy[hierarchy.index.isin(set(case_data['UID']))]['UID_parent']))].index)
    #print(case_uids)
    flow_uids=set(hierarchy[hierarchy['UID_parent'].isin(list(hierarchy[hierarchy.index.isin(set(flow_data['UID_work']).union(set(flow_data['UID_residence'])))]['UID_parent']))].index)
    #print(flow_uids)

    nodes = case_uids.intersection(flow_uids)
    # remove all parents    
    nodes = nodes - set(hierarchy[hierarchy.index.isin(nodes)]['UID_parent'])
    
    return nodes

# this function will filter the case for given nodes and adds sibling data if not explicitently existing
def prepare_covid(case_data, nodes, date, hierarchy, info):    

    # simply add nodes-existing_uids and set them to 0

    tmp = case_data[case_data['UID'].isin(nodes)]
    existing_uids = set(tmp['UID'])
   
    missing_uids = list(set(nodes)-existing_uids)
    #missing_uids = list(set(hierarchy[hierarchy['UID_parent'].isin(list(hierarchy[hierarchy.index.isin(set(tmp['UID']))]))].index) - existing_uids)

    locations = []
    for uid in missing_uids:
        locations.append(hierarchy.loc[uid]['name_child'])
        
    zeros = [0]*len(missing_uids)
    if len(missing_uids) > 0:
        print("Adding",len(missing_uids),"missing sibling(s):",missing_uids)
    data = tmp.append(pd.DataFrame({'UID':missing_uids,'location':locations,'date':[date]*len(missing_uids),
                                    'Confirmed':zeros,'Confirmed_daily':zeros,'Deaths':zeros,'Deaths_daily':zeros}))

    pop = []
    for uid in data['UID']:
        pop.append(info.loc[uid]['Population'])
    data['Population'] = pop

    return data

# for this one we simply have to filter according to the list of common nodes (we already ensured that all parents have been removed) and then normalize based on residence node
def prepare_flow(flow_data,nodes,info):

    flow_data = (flow_data[(flow_data['UID_work'].isin(nodes)) & (flow_data['UID_residence'].isin(nodes))]).copy()

    norm_flow = []
    for i, row in flow_data.iterrows():
        norm_flow.append(float(row['flow']) / info.loc[row['UID_residence']]['Population'])
    flow_data['norm_flow'] = norm_flow

    return flow_data

# for mobility data, parents have been already calculated, values for missing children have to be looked up from parents, also remove data for dates before initial date
def prepare_mobility(mobility_data, nodes, hierarchy):

    existing_data = set(mobility_data['UID'])
    #print('Existing:',existing_data)

    # first identify missing node data
    missing_nodes = nodes - existing_data
    
    # now walk the hierarchy to find a parent with data and use this
    #print('Missing:',missing_nodes)
    for uid in missing_nodes:
        #find parent with existing data
        uid_parent = hierarchy.loc[uid]['UID_parent']
        while (uid_parent not in existing_data):
            #print(uid_parent,hierarchy.loc[uid_parent])
            try:
                uid_parent = hierarchy.loc[uid_parent]['UID_parent']
            except:
                print('Error! no parent with data found!')
                uid_parent = None
                break
                
        if uid_parent is None:
            continue

        location = hierarchy.loc[uid]['name_child']

        tmp = mobility_data[mobility_data['UID']==uid_parent]
        mobility_data = mobility_data.append(pd.DataFrame({'UID':[uid]*len(tmp.index),'location':location*len(tmp.index),'date':tmp['date'],'mobility_type':tmp['mobility_type'],'mobility_change':tmp['mobility_change']}))

    # now remove everything not in the nodes list and return result
    return mobility_data[mobility_data['UID'].isin(nodes)]

if __name__ == "__main__":
  
    parser = argparse.ArgumentParser(description='Create init files for models/sims.')
    parser.add_argument('-r', '--region', help="specifies region to model. currently:{uk,us}", required=True)
    parser.add_argument('-i', '--date', help="specifies initial date (format:'%Y-%m-%d')", required=True)

    args = parser.parse_args()
    region = regions[args.region]
    date = args.date

    print('Creating ini files for region \'',region,'\' and date ',date)

    # load hierarchical data (MultiIndex: UID_parent,UID_child)
    print('Loading data ...')
    hierarchy=pd.read_csv(region['hierarchy'],dtype={'UID_parent':str,'UID_child':str},header=0)
    hierarchy = hierarchy.set_index(['UID_child'])
    #hierarchy.sort_index(inplace=True)

    info_data = pd.read_csv(region['info'],header=0,dtype={'UID':str})
    info_data = info_data.set_index('UID')
    flow_data = pd.read_csv(region['flow'],header=0,dtype={'UID_work':str,'UID_residence':str})
    case_data = pd.read_csv(region['cases'],header=0,dtype={'UID':str})
    case_data = case_data[case_data['date'] == date] #restrict to date of interest
    mobility_data = pd.read_csv(region['mobility'],header=0,dtype={'UID':str})

    nodes = determine_least_common_res(case_data,flow_data,hierarchy)
    print('Using',len(nodes),'nodes')
                           
    print('Creating ini files')
    prepare_covid(case_data,nodes,date,hierarchy,info=info_data).to_csv('covid.ini',index=False, columns=[*case_data.columns, 'Population'])
    print('Covid ini written')
    prepare_flow(flow_data,nodes,info_data).to_csv('flow.ini',index=False, columns=[*flow_data.columns, 'norm_flow'])
    print('Flow ini written')
    prepare_mobility(mobility_data,nodes,hierarchy).to_csv('mobility.ini',index=False, columns=mobility_data.columns)
    print('Mobility ini written')
    
    print("DONE - Ini files created")
