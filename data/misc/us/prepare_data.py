import pandas as pd
import numpy as np
from datetime import datetime, timedelta

lookup_table=pd.read_csv('RAW/UID_ISO_FIPS_LookUp_Table.csv')
lookup_table=lookup_table[(lookup_table['FIPS']>0)|(lookup_table['UID']==840)]
lookup_table.set_index(['FIPS'], inplace=True)
#print(lookup_table)

area_data=pd.read_excel('RAW/LND01.xls', sheet_name='Sheet 1',usecols=['STCOU','LND010190D'])
area_data.set_index('STCOU', inplace=True)
state_lookup_table=pd.read_csv('RAW/UID_ISO_FIPS_LookUp_Table.csv',index_col='Combined_Key')
state_lookup_table=state_lookup_table[(state_lookup_table['FIPS']>0) & (state_lookup_table['FIPS']<=56)]
#print(state_lookup_table.head())

hierarchy_columns = ['name_parent','UID_parent','name_child','UID_child']
hierarchy_data = {'UID_parent':[],'name_parent':[],'UID_child':[],'name_child':[]}

misc_columns = ['UID','location','Lat','Long','Population','Area']
misc_data = {'UID':[],'location':[],'Lat':[],'Long':[],'Population':[],'Area':[]}

print(area_data.head())
print(area_data.index)

for fips, row in lookup_table.iterrows():

    #print('processing ',fips)

    if row['UID']==840:
        print('Adding USA')
        misc_data['UID'].append(row['UID'])
        misc_data['location'].append('United States')
        misc_data['Long'].append(row['Long_'])
        misc_data['Lat'].append(row['Lat'])
        misc_data['Population'].append(row['Population'])
        misc_data['Area'].append(area_data.loc[0,'LND010190D']*2.59)
        continue
    elif fips < 1000:
        parent_loc = 'United States'
        parent_uid = '840'
        location = row['Province_State']
        if fips <= 56:
            #print(int(fips),  area_data.loc[int(fips*1000),'LND010190D']*2.59)
            area = area_data.loc[int(fips*1000),'LND010190D']*2.59
        else:
            area = -1
    elif fips < 60000:
        try:
            parent_loc = row['Province_State']
            parent_uid = '840'+str(int(state_lookup_table.loc[row['Province_State']+', US','FIPS'])).zfill(5)
            location = row['Admin2']
            if fips in area_data.index:
                 area = area_data.loc[fips,'LND010190D']*2.59
            else: 
                 area = -1
                 print("WARNING: No area data found:",fips)
        except:
            print("WARNING: Could not identify parent. Skipping entry:",parent_loc,area_data.loc[fips,'LND010190D']*2.59)
            continue
    else:
        continue

    uid = row['UID']
    long_ = row['Long_']
    lat = row['Lat']
    population = row['Population']

    hierarchy_data['UID_child'].append(uid)
    hierarchy_data['name_child'].append(location)
    hierarchy_data['UID_parent'].append(parent_uid)
    hierarchy_data['name_parent'].append(parent_loc)

    misc_data['UID'].append(uid)
    misc_data['location'].append(location)
    misc_data['Long'].append(long_)
    misc_data['Lat'].append(lat)
    misc_data['Population'].append(population)
    misc_data['Area'].append(area)

pd.DataFrame(hierarchy_data).to_csv('hierarchy.csv',index=False, columns=hierarchy_columns)
pd.DataFrame(misc_data).to_csv('info.csv',index=False, columns=misc_columns)

print("DONE")
    
    
    
