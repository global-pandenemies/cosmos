import pandas as pd
import numpy as np
from datetime import datetime, timedelta

lookup_fixing_uid = {    
    'S12000015':'S12000047',
    'S12000024':'S12000048',
    'S12000044':'S12000050',
    'S12000046':'S12000049',
    '95A':'95AA',
    '95B':'95BB',
    '95C':'95CC',
    '95D':'95DD',
    '95E':'95EE',
    '95F':'95FF',
    '95G':'95GG',
    '95H':'95HH',
    '95I':'95II',
    '95J':'95JJ',
    '95K':'95KK',
    '95L':'95LL',
    '95M':'95MM',
    '95N':'95NN',
    '95O':'95OO',
    '95P':'95PP',
    '95Q':'95QQ',
    '95R':'95RR',
    '95S':'95SS',
    '95T':'95TT',
    '95U':'95UU',
    '95V':'95VV',
    '95W':'95WW',
    '95X':'95XX',
    '95Y':'95YY',
    '95Z':'95ZZ',
}

xls = pd.ExcelFile('RAW/census-2011-population-5year-age-ukdistricts.xls')
pop_table=pd.read_excel(xls,'Data', header=[0],index_col="Code")
print(pop_table.columns)
hierarchy_data=pd.read_csv('hierarchy.csv',dtype={'UID_parent':str,'UID_child':str})
print(hierarchy_data.columns)

location_lookup = {}
for i,row in hierarchy_data.iterrows():
    location_lookup[row['UID_child']] = row['name_child']
    location_lookup[row['UID_parent']] = row['name_parent']

misc_columns = ['UID','location','Population']
misc_data = {'UID':[],'location':[],'Population':[]}

for uid, row in pop_table.iterrows():

    if uid!=uid: # if is NaN
        continue

    if uid in lookup_fixing_uid.keys():
        uid = lookup_fixing_uid[uid]        

    if uid not in set(hierarchy_data['UID_child']):
        if (uid == 'K02000001'):
            misc_data['UID'].append(uid)
            misc_data['location'].append('United Kingdom')
            misc_data['Population'].append(row['All usual residents'])        
        else:
            print("WARNING:",uid,"(",row['Area'],") not found in hierarchy. Skipping...")
    else:
        misc_data['UID'].append(uid)
        misc_data['location'].append(location_lookup[uid])
        misc_data['Population'].append(row['All usual residents'])

misc_data = pd.DataFrame(misc_data,columns=misc_columns)

# now construct missing parent nodes

print('==constructing parents')

existing_uids = set(misc_data['UID'])

def construct_node(uid):

    global misc_data, existing_uids

    #if uid in existing_uids:
    #    return

    children = set(hierarchy_data[hierarchy_data['UID_parent']==uid]['UID_child'])
    for child in children:
        construct_node(child)
    
    print('processing ...',uid)

    # if no children are existing, then return
    if len(children - existing_uids)==len(children):
        return

    # if only some children are existing, bail
    if len(children - existing_uids)>0:
        raise Exception('ERROR! Not all children are existing ... don\'t know what to do',uid,children - existing_uids);

    if uid in existing_uids:
        return
            
    #print(children)
    data_new = {'UID':[],'location':[],'Population':[]}

    # now simply sum up cases/deaths from/to all existing children    
    tmp = misc_data[misc_data['UID'].isin(children)]
        
    data_new['UID'].append(uid)
    data_new['location'].append(location_lookup[uid])                
    data_new['Population'].append(tmp['Population'].sum())

    misc_data = misc_data.append(pd.DataFrame(data_new,columns=misc_data.columns))

    existing_uids.add(uid)        
    return

construct_node('K02000001') #$ do for United Kingdom

pd.DataFrame(misc_data).to_csv('info.csv',index=False, columns=misc_columns)

print("DONE")
    
    
    
