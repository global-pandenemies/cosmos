import pandas as pd
import numpy as np
from datetime import datetime, timedelta

lookup_fixing_uid = {
    'E07000242':'E07000097',
    'E07000240':'E07000100',
    'E07000243':'E07000101',
    'E07000241':'E07000104',
    'E06000059':'E10000009',
    'E06000057':'E06000048',
    'E08000037':'E08000020',
}

confirmed_cases = pd.read_csv('RAW/coronavirus-cases.csv',header=0, delimiter=',', encoding="utf-8-sig")
#deaths = pd.read_csv('coronavirus-deaths.csv',header=0, delimiter=',', encoding="utf-8-sig")
#print(deaths.columns)

confirmed_cases_scot_wales = pd.read_csv('RAW/covid-19-cases-uk.csv',header=0, delimiter=',', encoding="utf-8-sig")
confirmed_cases_scot_wales = confirmed_cases_scot_wales[confirmed_cases_scot_wales['AreaCode'].notnull()]
confirmed_cases_scot_wales = confirmed_cases_scot_wales[confirmed_cases_scot_wales['AreaCode'].str.startswith(tuple(['S','W']))]
confirmed_cases_scot_wales.head()

hierarchy_data=pd.read_csv('../../misc/uk/hierarchy.csv')
print(hierarchy_data.index)

# first collect all locations
areas_ = set(confirmed_cases['Area code'])  #.intersection(set(deaths['Area code']))
areas_scot_wales_ = set(confirmed_cases_scot_wales['AreaCode'])

# remove area if children exist (can do this for england & scot/wales seperately). We will reconstruct them all later
areas = areas_
#areas = []
#for area in areas_:    
#    common = set(hierarchy_data[hierarchy_data['UID_parent']==area]['UID_child']).intersection(areas)
#    if len(common)==0:
#        areas.append(area)
#    else:
#        print('Area skipped. Children found in data:',area,common)

print(len(areas))

areas_scot_wales = []
for area in areas_scot_wales_:    
    common = set(hierarchy_data[hierarchy_data['UID_parent']==area]['UID_child']).intersection(areas_scot_wales_)
    if len(common)==0:
        areas_scot_wales.append(area)
    else:
        print('Area skipped. Children found in data:',area,common)

print(len(areas_scot_wales))

#now create timeline for each area
columns = ['UID','location','date','Confirmed_daily','Confirmed','Deaths_daily','Deaths']
final_data = {'UID':[],'location':[],'date':[],'Confirmed':[],'Deaths':[], 'Confirmed_daily':[], 'Deaths_daily':[]}

delta_1day = timedelta(days=1)
start_date = datetime.strptime(min(confirmed_cases['Specimen date']),"%Y-%m-%d")
#start_date = datetime.strptime(min(min(confirmed_cases['Specimen date']),min(deaths['Reporting date'])),"%Y-%m-%d") # find first day in data
end_date = datetime.strptime(min(max(confirmed_cases['Specimen date']),max(confirmed_cases_scot_wales['Date'])),"%Y-%m-%d") # find first day in data
#end_date = datetime.strptime(min(max(confirmed_cases['Specimen date']),max(deaths['Reporting date'])),"%Y-%m-%d") # find first day in data
print('start:',start_date,'last:',end_date)

dates = []

while start_date <= end_date:
    dates.append(start_date.strftime("%Y-%m-%d"))
    start_date += delta_1day

print(len(dates))

# TOOD could be optimized 
location_lookup = {}
for i,row in hierarchy_data.iterrows():
    location_lookup[row['UID_child']] = row['name_child']
    location_lookup[row['UID_parent']] = row['name_parent']

for area in areas:
    data_cases = confirmed_cases[confirmed_cases['Area code']==area].sort_values('Specimen date')

    if (data_cases.count == 0):
        continue

    i_date = 0
    n=0

    final_data['date'] += dates
    if area in lookup_fixing_uid:
        area = lookup_fixing_uid[area]    
    final_data['UID'] += [area]*len(dates)
    final_data['location'] += [location_lookup[area]]*len(dates)

    for i,row in data_cases.iterrows():
        
        while ((i_date < len(dates) and dates[i_date] != row['Specimen date'])):
            if dates[i_date] > row['Specimen date']:
                print('ERROR! sorting wrong?')
            final_data['Confirmed_daily'].append(0)
            final_data['Confirmed'].append(n)
            i_date+=1

        if i_date < len(dates):

            if (n+row['Daily lab-confirmed cases'])!=int(row['Cumulative lab-confirmed cases']):
                print("WARNING! Inconsistency found in cases!",row['Area name'],row['Daily lab-confirmed cases'],row['Area code'],n,row['Cumulative lab-confirmed cases'],row['Specimen date'])
                row['Daily lab-confirmed cases']=row['Cumulative lab-confirmed cases']-n
            final_data['Confirmed_daily'].append(int(row['Daily lab-confirmed cases']))
            final_data['Confirmed'].append(row['Cumulative lab-confirmed cases'])
            n = int(row['Cumulative lab-confirmed cases'])
            i_date+=1

    #data_deaths = deaths[deaths['Area code']==area].sort_values('Reporting date')
    while i_date < len(dates):
        final_data['Confirmed_daily'].append(0)
        final_data['Confirmed'].append(n)
        i_date+=1

    final_data['Deaths_daily'] += [np.nan]*len(dates)
    final_data['Deaths'] += [np.nan]*len(dates)

    #i_date = 0
    #n=0    

    #for i,row in data_deaths.iterrows():

    #    while ((i_date < len(final_data['date']) and final_data['date'][i_date] != row['Reporting date'])): 
    #        if final_data['date'][i_date] > row['Reporting date']:
    #            print('ERROR! sorting wrong?')
    #        final_data['Deaths_daily'].append(0)
    #        final_data['Deaths'].append(n)
    #        i_date+=1
    #    if (n+row['Daily hospital deaths'])!=row['Cumulative hospital deaths']:
    #        print("WARNING! Inconsistency found in deaths!",i,row['Area code'],n,row['Daily hospital deaths'],row['Cumulative hospital deaths'])
    #    final_data['Confirmed_daily'].append(row['Daily hospital deaths'])
    #    final_data['Confirmed'].append(row['Cumulative hospital deaths'])
    #    n = row['Cumulative hospital deaths']
    #    i_date+=1

for area in areas_scot_wales:
    data_cases = confirmed_cases_scot_wales[confirmed_cases_scot_wales['AreaCode']==area].sort_values('Date')

    if (data_cases.count == 0):
        continue

    final_data['date'] += dates
    if area in lookup_fixing_uid:
        area = lookup_fixing_uid[area]
    final_data['UID'] += [area]*len(dates)
    final_data['location'] += [location_lookup[area]]*len(dates)

    i_date = 0
    n=0

    for i,row in data_cases.iterrows():

        while (i_date < len(dates) and dates[i_date] != row['Date']):
            if dates[i_date] > row['Date']:
                print('ERROR! sorting wrong?')
            final_data['Confirmed_daily'].append(0)
            final_data['Confirmed'].append(n)
            i_date+=1        

        if (i_date < len(dates)):
            try:
                m=int(row['TotalCases'])
            except:
                print("WARNING: Invalid value. Trying to fix it by assuming no change: ",row['Area'],row['Date'],row['TotalCases'])
                m=n

            final_data['Confirmed_daily'].append(m-n)
            final_data['Confirmed'].append(m)

            i_date+=1

    while i_date < len(dates):
        final_data['Confirmed_daily'].append(0)
        final_data['Confirmed'].append(n)
        i_date+=1

    final_data['Deaths_daily'] += [np.nan]*len(dates)
    final_data['Deaths'] += [np.nan]*len(dates)

    #data_deaths = deaths[deaths['Area code']==area].sort_values('Reporting date')
    
print(len(final_data['date']),len(final_data['UID']),len(final_data['location']),len(final_data['Confirmed']),len(final_data['Confirmed_daily']),len(final_data['Deaths']),len(final_data['Deaths_daily']))
final_data = pd.DataFrame(final_data,columns=['UID','location','date','Confirmed','Confirmed_daily','Deaths','Deaths_daily'])

# now construct parent nodes again where available
for date in dates:

    print('==constructing parents:',date)

    data_date = final_data[final_data['date']==date]
    existing_uids = set(data_date['UID'])

    def construct_node(uid):

        global final_data, data_date, existing_uids

        if uid in existing_uids:
            return

        children = set(hierarchy_data[hierarchy_data['UID_parent']==uid]['UID_child'])
        for child in children:
            construct_node(child)
    
        print('processing ...',uid)

        # if no children are existing, then return
        if len(children - existing_uids)==len(children):
            return

        # if only some children are existing, bail
        if len(children - existing_uids)>0:
            raise Exception('ERROR! Not all children are existing ... don\'t know what to do',uid,children - existing_uids);
            
        #print(children)
        data_new = {'UID':[],'location':[],'date':[],'location':[],'Deaths':[],'Deaths_daily':[],'Confirmed':[],'Confirmed_daily':[]}

        # now simply sum up cases/deaths from/to all existing children    
        tmp = data_date[data_date['UID'].isin(children)]
        
        data_new['UID'].append(uid)
        data_new['location'].append(location_lookup[uid])                
        data_new['date'].append(date)
        data_new['Confirmed'].append(tmp['Confirmed'].sum())
        data_new['Confirmed_daily'].append(tmp['Confirmed_daily'].sum())
        data_new['Deaths'].append(tmp['Deaths'].sum())
        data_new['Deaths_daily'].append(tmp['Deaths_daily'].sum())

        data_date = data_date.append(pd.DataFrame(data_new,columns=data_date.columns))
        final_data = final_data.append(pd.DataFrame(data_new,columns=final_data.columns))

        existing_uids.add(uid)        
        return

    construct_node('K03000001') #$ do for Great Britain

#print(len(final_data['date']),len(final_data['UID']),len(final_data['location']),len(final_data['Confirmed']),len(final_data['Confirmed_daily']),len(final_data['Deaths']),len(final_data['Deaths_daily']))
final_data.to_csv('timeline.csv',index=False, columns=columns)

print("DONE")
