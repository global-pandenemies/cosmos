import pandas as pd
import numpy as np
from datetime import datetime, timedelta

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

confirmed_cases = pd.read_csv('RAW/covid_confirmed_usafacts.csv',header=0, delimiter=',', encoding="utf-8-sig")
deaths = pd.read_csv('RAW/covid_deaths_usafacts.csv',header=0, delimiter=',', encoding="utf-8-sig")

hierarchy_data=pd.read_csv('../../misc/us/hierarchy.csv', dtype={'UID_child':str,'UID_parent':str})

# TOOD could be optimized
location_lookup = {}
for i,row in hierarchy_data.iterrows():
    location_lookup[row['UID_child']] = row['name_child']
    location_lookup[row['UID_parent']] = row['name_parent']

lookup_table=pd.read_csv('../../misc/us/RAW/UID_ISO_FIPS_LookUp_Table.csv')
#lookup_table=lookup_table[lookup_table['FIPS'].notna()]
lookup_table.set_index(['FIPS'], inplace=True)
#print(lookup_table.index)

state_lookup_table=pd.read_csv('../../misc/us/RAW/UID_ISO_FIPS_LookUp_Table.csv',index_col='Combined_Key')
state_lookup_table=state_lookup_table[(state_lookup_table['FIPS']>0) & (state_lookup_table['FIPS']<=56)]

date_cols = confirmed_cases.columns[4:]

# correct fips for states

unallocated_cases = []

for i, row in confirmed_cases.iterrows():

    if row['countyFIPS'] == 1: #should only occur for NY City and will be ignored
        confirmed_cases.at[i,'countyFIPS']=-1
        #confirmed_cases.at[i,'countyFIPS']=int(state_lookup_table.loc[states[row['State']]+', US','FIPS'])*1000

#print(list(confirmed_cases['countyFIPS']))

for i, row in confirmed_cases.iterrows():

    if row['countyFIPS'] == 0: # unallocated statewide cases (will be only added to state stats)
        uid = state_lookup_table.loc[states[row['State']]+', US','FIPS']
        # check if fips already exists (in this case, our script won't work)
        if uid in list(confirmed_cases['countyFIPS']):
            raise Exception('Error! (Unallocated) cases data for this area already exists! Exiting ... :',row['State'],states[row['State']],uid)

        confirmed_cases.at[i,'countyFIPS']=uid
        confirmed_cases.at[i,'County Name']=states[row['State']]
        unallocated_cases.append(uid)

for i, row in deaths.iterrows():

    if row['countyFIPS'] == 1: #should only occur for NY City and will be ignored
        deaths.at[i,'countyFIPS'] = -1
        #deaths.at[i,'countyFIPS']=int(state_lookup_table.loc[states[row['State']]+', US','FIPS'])*1000

for i, row in confirmed_cases.iterrows():

    if row['countyFIPS'] == 0: # unallocated statewide cases (will be only added to state stats)
        uid = state_lookup_table.loc[states[row['State']]+', US','FIPS']

        # check if fips already exists (in this case, our script won't work)
        if uid in list(deaths['countyFIPS']):
            raise Exception('Error! (Unallocated) deaths data for this area already exists! Exiting ... :',uid)

        deaths.at[i,'countyFIPS']=state_lookup_table.loc[states[row['State']]+', US','FIPS']
        unallocated_cases.append(uid)

unallocated_cases = set(unallocated_cases)

# merge dataframes on fips

merged_data = confirmed_cases.merge(deaths, on='countyFIPS',how='left', suffixes=('','_deaths')).fillna(0) # TODO how should be outer, but does it still work with suffixes?

columns = ['UID','location','date','Confirmed_daily','Confirmed','Deaths_daily','Deaths']
data_new = {'UID':[],'location':[],'date':[],'Confirmed':[],'Deaths':[], 'Confirmed_daily':[], 'Deaths_daily':[]}

for i, row in merged_data.iterrows():

    fips = row['countyFIPS']

    print('processing ',fips)

    if fips < 100:
        loc_type = 'parent'
        parent_loc = 'United States'
        parent_uid = '840'
    else:
        loc_type = 'child'
        parent_loc = states[row['State']]
        parent_uid = '840'+str(state_lookup_table.loc[states[row['State']]+', US','FIPS']).zfill(5)

    location = row['County Name']

    if fips not in lookup_table.index:
        print('==WARNING: FIPS metadata not found: ',fips)
        uid = '840'+str(fips).zfill(5)
        print('UID:',uid)
    else:
        uid = str(lookup_table.loc[fips,'UID'])

    for date in date_cols:

        prev_date = (datetime.strptime(date,"%m/%d/%y")-timedelta(days=+1)).strftime("%-m/%-d/%y")
        if prev_date not in date_cols:
           prev_date=None

        data_new['UID'].append(uid)
        data_new['location'].append(location)
        data_new['date'].append((datetime.strptime(date,"%m/%d/%y")).strftime("%Y-%m-%d"))
        if prev_date is not None:
            data_new['Confirmed_daily'].append(int(row[date]-row[prev_date]))
        #elif row[date]==0:
        #    data_new['Confirmed_daily'].append(0)
        else:
            data_new['Confirmed_daily'].append(row[date])
        data_new['Confirmed'].append(row[date])
        if prev_date is not None:
            data_new['Deaths_daily'].append(int(row[date+'_deaths']-row[prev_date+'_deaths']))
        #elif row[date+'_deaths']==0:
        #    data_new['Deaths_daily'].append(0)
        else:
            data_new['Deaths_daily'].append(row[date+'_deaths'])
        data_new['Deaths'].append(row[date+'_deaths'])


final_data = pd.DataFrame(data_new)
final_data = final_data.set_index('UID')
print(final_data)

#now add parents and add unallocated cases if necessary
# now construct parent nodes again where available

dates = set(final_data['date'])

done = set()

#print(set(final_data.index))
#print(set(hierarchy_data['UID_child'])-set(final_data.index))

def construct_node(uid):

    global final_data, done, unallocated_cases

    if uid in done:
        raise Exception('Error! We assume a striclty tree-like graph i.e. only a single path to each node from the base i.e. the US node')
    
    #print (hierarchy_data['UID_parent'],hierarchy_data[hierarchy_data['UID_parent']==uid])
    children = set(hierarchy_data[hierarchy_data['UID_parent']==uid]['UID_child'])
    for child in children:
        construct_node(child)

    print('processing ...',uid)
    #print(uid in set(final_data.index), children,children.intersection(done))

    # check if node needs to be (re)constructed i.e. if it did not exist yet and none of their children exists, return
    if uid not in list(final_data.index) and len(children.intersection(done))==0:        
        return

    print(uid, unallocated_cases)
    if uid not in unallocated_cases and len(children)==0:
        done.add(uid)
        return

    data_new = {'UID':[],'location':[],'date':[],'location':[],'Deaths':[],'Deaths_daily':[],'Confirmed':[],'Confirmed_daily':[]}

    for date in dates:
        
        print('==constructing parents:',date)

        # now simply sum up cases/deaths from/to all existing children and add unallocated cases/deaths

        #first check if unallocated data exists and only one if so

        if uid in final_data.index:

            #print(final_data[final_data.index==uid])
            tmp = final_data[(final_data.index==uid)&(final_data['date']==date)]

            if tmp.shape[0]>1:
                raise Exception('Error! More than one existing node found:',uid)

            unalloc_cases = tmp.loc[uid,'Confirmed']
            unalloc_cases_daily = tmp.loc[uid,'Confirmed_daily']
            unalloc_deaths = tmp.loc[uid,'Deaths']
            unalloc_deaths_daily = tmp.loc[uid,'Deaths_daily']
        else:            
            unalloc_cases = unalloc_cases_daily = unalloc_deaths = unalloc_deaths_daily = 0
        
        tmp = final_data[(final_data.index.isin(children))&(final_data['date']==date)]

        data_new['UID'].append(uid)
        data_new['location'].append(location_lookup[uid])                
        data_new['date'].append(date)
        #print(tmp['Confirmed'],tmp['Confirmed'].sum(),unalloc_cases)
        data_new['Confirmed'].append(int(tmp['Confirmed'].sum()+unalloc_cases))
        data_new['Confirmed_daily'].append(int(tmp['Confirmed_daily'].sum()+unalloc_cases_daily))
        data_new['Deaths'].append(int(tmp['Deaths'].sum()+unalloc_deaths))
        data_new['Deaths_daily'].append(int(tmp['Deaths_daily'].sum()+unalloc_deaths_daily))

    # now remove existing data and add new node
    final_data['UID'] = final_data.index
    final_data = final_data[final_data.index!=uid].append(pd.DataFrame(data_new)).set_index('UID')
    #print(final_data)

    done.add(uid)
    return

construct_node('840') # do for United States

final_data['UID'] = final_data.index
final_data['Confirmed'] = final_data['Confirmed'].astype(int)
final_data['Confirmed_daily'] = final_data['Confirmed_daily'].astype(int)
final_data['Deaths'] = final_data['Deaths'].astype(int)
final_data['Deaths_daily'] = final_data['Deaths_daily'].astype(int)

final_data.sort_values(by=['UID','date'],inplace=True);

final_data.to_csv('timeline.csv',index=False, columns=columns)

print("DONE")    
    
    
