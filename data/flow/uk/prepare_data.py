import pandas as pd
import numpy as np
import math
import sys

lookup_fixing_uid = {
    'E07000242':'E07000097',
    'E07000240':'E07000100',
    'E07000243':'E07000101',
    'E07000241':'E07000104',
    'E06000059':'E10000009',
    'E06000057':'E06000048',
    'E08000037':'E08000020',
}

src_cols = ['ResidenceLADCode','ResidenceLAD','WorkLADCode','WorkLAD','AllModes']
data_flow = pd.read_csv('RAW/20200416_1135_CensusTravelLAD.csv', header=0, delimiter=',', encoding="utf-8-sig")[src_cols]
#df.insert(0, 'New_ID', range(0, len(df)))
hierarchy_data=pd.read_csv('../../misc/uk/hierarchy.csv',index_col='UID_child',header=0)

# flow & lookup table for scotland
data_lookup = pd.read_csv('../../misc/uk/RAW/auth2lad_2011.csv', header=0, delimiter=',', encoding="utf-8-sig")
data_flow2 = pd.read_csv('RAW/ONS_WU03UK.csv', header=0, skipfooter=6, delimiter=',', encoding="utf-8-sig",skiprows=10, index_col=0, engine='python').dropna()
cols = list(data_flow2.columns)
cols[0] = 'UID_work'
data_flow2.columns = cols

# TOOD could be optimized
location_lookup = {}
for i,row in hierarchy_data.iterrows():
    location_lookup[i] = row['name_child']
    location_lookup[row['UID_parent']] = row['name_parent']

hierarchy_uids = set(location_lookup.keys())

# first clean data (some rows have invalid entries coz the uid ended up in second column, also add up same-direction flows)

#print(data_flow.head())

errors = {}

for i,row in data_flow.iterrows():

    uid_residence = row['ResidenceLADCode']
    uid_work = row['WorkLADCode']
    
    if row['ResidenceLADCode'] not in hierarchy_uids:        
        #first check if uid in next column i.e in 'ResidenceLAD'
        if row['ResidenceLAD'] in hierarchy_uids:            
            data_flow.at[i,'ResidenceLADCode'] = row['ResidenceLAD']
            data_flow.at[i,'ResidenceLAD'] = location_lookup[row['ResidenceLAD']]
        elif row['ResidenceLADCode'] in lookup_fixing_uid:
            data_flow.at[i,'ResidenceLADCode'] = lookup_fixing_uid[row['ResidenceLADCode']]
        elif row['ResidenceLAD'] in lookup_fixing_uid:
            data_flow.at[i,'ResidenceLADCode'] = lookup_fixing_uid[row['ResidenceLAD']]
            data_flow.at[i,'ResidenceLAD'] = location_lookup[lookup_fixing_uid[row['ResidenceLAD']]]
        else:
            #print("Error! could not identify residence:",uid_residence,row['ResidenceLAD'])       
            if (uid_residence in errors.keys()):
                errors[uid_residence].append(i)
            else:
                errors[uid_residence]=[i]

    # same for work place
    if uid_work not in hierarchy_uids:
        #first check if uid in next column i.e in 'WorkLAD'
        if row['WorkLAD'] in hierarchy_uids:
            data_flow.at[i,'WorkLADCode'] = row['WorkLAD']
            data_flow.at[i,'WorkLAD'] = location_lookup[row['WorkLAD']]
        elif row['WorkLADCode'] in lookup_fixing_uid:
            data_flow.at[i,'WorkLADCode'] = lookup_fixing_uid[row['WorkLADCode']]
        elif row['WorkLAD'] in lookup_fixing_uid:
            data_flow.at[i,'WorkLADCode'] = lookup_fixing_uid[row['WorkLAD']]
            data_flow.at[i,'WorkLAD'] = location_lookup[lookup_fixing_uid[row['WorkLAD']]]
        else:
            #print("Error! could not identify workplace:",uid_work,row['WorkLAD'])
            if (uid_work in errors.keys()):
                errors[uid_work].append(i)
            else:
                errors[uid_work]=[i]

for uid in errors.keys():
    print("Error! could not identify place:",uid,errors[uid])

#drop erroneous lines, we also drop the home workers (OD0000001) & scillies
print(data_flow.shape)
data_flow = data_flow[(~data_flow['ResidenceLADCode'].isin(errors.keys()))&(~data_flow['WorkLADCode'].isin(errors.keys()))]
print(data_flow.shape)

data_flow = data_flow.groupby(['ResidenceLADCode','WorkLADCode'])['AllModes'].sum().reset_index()

print(data_flow.head())

# now add data for scotland from second source

code_table = {}
code_table_inv = {}

for i,row in data_lookup.iterrows():
    if row['to_supplied_code'] in code_table.keys():
        if code_table[row['to_supplied_code']]==row['to_opcs_code']:
            continue
        raise Exception('ERROR. LAD already exists',row['to_supplied_code'],code_table[row['to_supplied_code']],row['to_opcs_code'])
    elif row['to_opcs_code'] in code_table_inv.keys():
        if code_table_inv[row['to_opcs_code']]==row['to_supplied_code']:
            continue
        raise Exception('ERROR. LAD already exists',row['to_opcs_code'],row['to_supplied_code'])
    else:
        code_table[row['to_supplied_code']] = row['to_opcs_code']
        code_table_inv[row['to_opcs_code']] = row['to_supplied_code']

# correcting code_table by comparing names
missing = list(set(data_flow2['UID_work'])-set(code_table.keys()))
# for all missing, we try first to assume that they translate trivially (e.g. for scottish councils)
for i in missing:
    code_table[i] = i
    code_table_inv[i] = i

#now we calculate missing again
missing = list(set(hierarchy_data.index)-set(hierarchy_data['UID_parent'])-set(code_table_inv.keys()))

still_missing = []
for i in missing:
    try:
        print('@',i, '|',hierarchy_data.loc[i,'name_child'],'|',data_flow2.loc[hierarchy_data.loc[i,'name_child'],'UID_work'])
        del code_table_inv[code_table[data_flow2.loc[hierarchy_data.loc[i,'name_child'],'UID_work']]]
        code_table[data_flow2.loc[hierarchy_data.loc[i,'name_child'],'UID_work']]=i
        code_table_inv[i]=data_flow2.loc[hierarchy_data.loc[i,'name_child'],'UID_work']
    except:
        print('Could not find:',i, hierarchy_data.loc[i,'name_child'])
        still_missing.append(i)

#sanity check (i.e. are all entries in hierarchy?)
#missing = set(code_table_inv.keys()) - (set(hierarchy_data.index))
#if len(missing)>0:
#    raise Exception('Entries missing in hierarchy',missing)

city_westm = {'cauth':'E41000324','uids':['E09000001','E09000033']}

tmp=data_flow.set_index(['WorkLADCode','ResidenceLADCode'])
tmp2=data_flow2.set_index('UID_work')

for uid in hierarchy_data.index:
    for uid2 in hierarchy_data.index:
        if uid in city_westm['uids'] or uid2 in city_westm['uids']:
            continue
        try:
            if int(tmp.loc[(uid,uid2)]['AllModes']) != int(tmp2.loc[code_table_inv[uid],code_table_inv[uid2]]):
                print('==DIFFERENCE!',uid2,uid)
                print(tmp.loc[(uid,uid2)]['AllModes'],tmp2.loc[code_table_inv[uid],code_table_inv[uid2]])
        except Exception as e :
            #print('ERROR!', str(e))
            pass

if len(set(still_missing) - set(city_westm['uids']))>0:
    raise Exception('still some unhandled problems here. No idea how to fix them. Bailing out!',set(still_missing) - set(city_westm['uids']))

# we estimate the commuter share for each city of london & westminster from the split observed in the UK (excluding intra-district flows)
flow_share = {}
total = 0
for uid in city_westm['uids']:
    flow_share[uid] = sum(data_flow[(data_flow['ResidenceLADCode']==uid)&(~data_flow['WorkLADCode'].isin(city_westm['uids']))]['AllModes'])
total = sum(flow_share.values())
for uid in city_westm['uids']:
    flow_share[uid] /= total

print('Flow_share:',flow_share)

# now add scottish flows

uids_scotland = set(hierarchy_data[hierarchy_data.index.str.startswith('S12')].index)
#print(uids_scotland)

# check if data for all of scotland is there
missing = uids_scotland - set(code_table_inv.keys())
if len(missing)>0:
    raise Exception('Some scottish data is still missing:',missing)

if len(set(data_flow['ResidenceLADCode']).intersection(uids_scotland))>0:
    raise Exception('Already some Scottish data here. Not expecting this:',set(data_flow['ResidenceLADCode']).intersection(uids_scotland))

data_flow2 = data_flow2.set_index('UID_work')

# now insert the new data
data_new = {'ResidenceLADCode':[],'WorkLADCode':[],
            #'ResidenceLAD':[],'WorkLAD':[],
            'AllModes':[]}
for scot in uids_scotland:
    for uid in set(data_flow['ResidenceLADCode']).union(uids_scotland):
        if uid in city_westm['uids']:                                 
            #print('Processing',scot,uid,city_westm['cauth'],code_table_inv[scot])

            if data_flow2.loc[city_westm['cauth'],code_table_inv[scot]] >0:
                data_new['ResidenceLADCode'].append(scot)
                data_new['WorkLADCode'].append(uid)
                #data_new['ResidenceLAD'].append(hierarchy_data.loc[scot,'name_child'])
                #data_new['WorkLAD'].append(hierarchy_data.loc[uid,'name_child'])
                data_new['AllModes'].append(round(flow_share[uid]*data_flow2.loc[city_westm['cauth'],code_table_inv[scot]]))
                
            if data_flow2.loc[code_table_inv[scot],city_westm['cauth']] >0:
                data_new['ResidenceLADCode'].append(uid)
                data_new['WorkLADCode'].append(scot)
                #data_new['ResidenceLAD'].append(hierarchy_data.loc[uid,'name_child'])
                #data_new['WorkLAD'].append(hierarchy_data.loc[scot,'name_child'])
                data_new['AllModes'].append(round(flow_share[uid]*data_flow2.loc[code_table_inv[scot],city_westm['cauth']]))
        else:

            if data_flow2.loc[code_table_inv[uid],code_table_inv[scot]] >0:
                data_new['ResidenceLADCode'].append(scot)
                data_new['WorkLADCode'].append(uid)
                #data_new['ResidenceLAD'].append(location_lookup[scot])
                #data_new['WorkLAD'].append(location_lookup[uid])
                data_new['AllModes'].append(data_flow2.loc[code_table_inv[uid],code_table_inv[scot]])
            
            if data_flow2.loc[code_table_inv[scot],code_table_inv[uid]] >0 and uid not in uids_scotland:
                data_new['ResidenceLADCode'].append(uid)
                data_new['WorkLADCode'].append(scot)
                #data_new['ResidenceLAD'].append(location_lookup[uid])
                #data_new['WorkLAD'].append(location_lookup[scot])
                data_new['AllModes'].append(data_flow2.loc[code_table_inv[scot],code_table_inv[uid]])

#print(data_new)
print(len(data_new['AllModes']))

# add to main flow data set
data_flow = data_flow.append(pd.DataFrame(data_new,columns=data_flow.columns))
print(data_flow.shape)

# run some sanity checks

# first check if parents and children exist both (if so, bail out, coz we would need further checks or merging)
existing_uids = set(data_flow['ResidenceLADCode']).union(set(data_flow['WorkLADCode']))
print('EXISTING:',existing_uids)

for uid in existing_uids:    
    if hierarchy_data.loc[uid,'UID_parent'] in existing_uids:
        raise Exception("ERROR: parent exists in flow data as well: "+hierarchy_data.loc[uid,'UID_parent']+uid)

# now we merge nodes to build herarchy upwards (missing children are treated as 0)

# first create a todo list (i.e. all parents who have children with incoming/outgoing flow)
#uids_to_do = existing_uids
#delta_uids = existing_uids

#while True:
#    print(delta_uids)
#    if len(delta_uids)==0:
#        break
#    tmp = set(hierarchy_data[hierarchy_data.index.isin(delta_uids)]['UID_parent'])
#    delta_uids = tmp - uids_to_do
#    uids_to_do = uids_to_do.union(tmp)

#print(uids_to_do)
    
def construct_node(uid):

    global data_flow, existing_uids

    if uid in existing_uids:
        return [uid]

    ignore_list = []
    children = set(hierarchy_data[hierarchy_data['UID_parent']==uid].index)
    for child in children:
        ignore_list += construct_node(child)
    
    print('processing ...',uid)

    print(children)
    data_new = {'ResidenceLADCode':[],'WorkLADCode':[],#'ResidenceLAD':[],'WorkLAD':[],
                'AllModes':[]}

    # now simply sum up flows from/to all existing children    
    workLAD_data = data_flow[data_flow['ResidenceLADCode'].isin(children)]
    already_done = set(data_flow[data_flow['ResidenceLADCode']==uid]['WorkLADCode'])
    print(list(set(workLAD_data['WorkLADCode'])-already_done))
    print(already_done)
    print(workLAD_data)
    for workLAD in set(set(workLAD_data['WorkLADCode'])-already_done):
        flow = sum(workLAD_data[workLAD_data['WorkLADCode']==workLAD]['AllModes'])
        if flow >0:
            data_new['ResidenceLADCode'].append(uid)
            data_new['WorkLADCode'].append(workLAD)
            #data_new['ResidenceLAD'].append(location_lookup[uid])
            #data_new['WorkLAD'].append(location_lookup[workLAD])
            data_new['AllModes'].append(flow)

    data_flow = data_flow.append(pd.DataFrame(data_new,columns=data_flow.columns))

    data_new = {'ResidenceLADCode':[],'WorkLADCode':[],#'ResidenceLAD':[],'WorkLAD':[],
                'AllModes':[]}
    
    # for work places
    resLAD_data = data_flow[data_flow['WorkLADCode'].isin(children)]
    already_done = set(data_flow[(data_flow['WorkLADCode']==uid)]['ResidenceLADCode'])
    for resLAD in (set(resLAD_data['ResidenceLADCode'])-already_done):
        flow = sum(resLAD_data[resLAD_data['ResidenceLADCode']==resLAD]['AllModes'])
        if flow > 0 and resLAD not in already_done:
            data_new['ResidenceLADCode'].append(resLAD)
            data_new['WorkLADCode'].append(uid)
            #data_new['ResidenceLAD'].append(location_lookup[resLAD])
            #data_new['WorkLAD'].append(location_lookup[uid])
            data_new['AllModes'].append(flow)

    data_flow = data_flow.append(pd.DataFrame(data_new,columns=data_flow.columns))
    existing_uids.add(uid)

    ignore_list.append(uid)
    return ignore_list

construct_node('K02000001')

# now remove again flow between parents and children (needed that previously for calculation, now removing it for final dataset)

def reduce_node(uid):
    
    global data_flow

    children = set(hierarchy_data[hierarchy_data['UID_parent']==uid].index)

    removal_list = []
    for child in children:
        removal_list += reduce_node(child)

    data_flow = data_flow[(data_flow['ResidenceLADCode']!=uid)|(~data_flow['WorkLADCode'].isin(removal_list))]
    data_flow = data_flow[(data_flow['WorkLADCode']!=uid)|(~data_flow['ResidenceLADCode'].isin(removal_list))]

    removal_list.append(uid)
    return removal_list

print(data_flow.shape)

reduce_node('K02000001')

print(data_flow.shape)

# assuming that everything is clean and completed, we now collect the columns we want to use

data_flow = pd.DataFrame({'UID_residence':data_flow['ResidenceLADCode'],'loc_residence':[location_lookup[i] for i in data_flow['ResidenceLADCode']],'UID_work':data_flow['WorkLADCode'],'loc_work':[location_lookup[i] for i in data_flow['WorkLADCode']],'flow':data_flow['AllModes']}, columns=['UID_residence','loc_residence','UID_work','loc_work','flow'])
print(data_flow.columns)

data_flow.to_csv('flow_graph.csv',index=False, columns=data_flow.columns)
    
    
    
