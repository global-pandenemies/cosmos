import pandas as pd
import numpy as np
import math

data_flow = pd.read_excel('RAW/table2.xlsx', header=[5, 6], skipfooter=2) #, dtype={('Residence','Country FIPS Code'):object})
lookup_table=pd.read_csv('../../misc/us/RAW/UID_ISO_FIPS_LookUp_Table.csv',index_col='Combined_Key')
#TODO timeline = pd.read_csv('../../covid/us/timeline.csv')

#print(data_flow.tail())

#print(data_flow.loc[:,[('Residence','County FIPS Code'),('Residence','County Name'),('Place of Work','County FIPS Code'),('Place of Work','County Name')]])

final_cols = ['UID_residence','loc_residence','UID_work','loc_work','flow','error']
tmp_data = {'UID_residence':[],'loc_residence':[],'UID_work':[],'loc_work':[],'flow':[],'error':[]}

for state_fips_residence,row in data_flow.iterrows():

    if math.isnan(row[('Place of Work','County FIPS Code')]):

        #TODO
        continue

        # try to identify FIPS for region using lookup table (probably country)

        try:
            uid = lookup_table.loc[row[('Place of Work','State Name')],'FIPS']
        except:
            print("==Error: Failed to identify FIPS",row[('Place of Work','State Name')])
            uid=""

        tmp_data['UID_work'].append(uid)
        tmp_data['loc_work'].append(row[('Place of Work','State Name')])

    else:
        tmp_data['UID_work'].append('840'+str(int(row[('Place of Work','State FIPS Code')])).zfill(2)+str(int(row[('Place of Work','County FIPS Code')])).zfill(3))
        tmp_data['loc_work'].append(row[('Place of Work','County Name')])

    tmp_data['UID_residence'].append('840'+str(int(state_fips_residence)).zfill(2)+str(int(row[('Residence','County FIPS Code')])).zfill(3))
    tmp_data['loc_residence'].append(row[('Residence','County Name')])

    tmp_data['flow'].append(row[('Commuting Flow','Workers in Commuting Flow')])
    tmp_data['error'].append(row[('Commuting Flow',' Margin of Error')])
    

df = pd.DataFrame(tmp_data)

# prune / merge flows where necessary (i.e. source/destination not in sim volume or at different resolution)

#TODO

# write output

df.to_csv('flow_graph.cvs',columns=final_cols, index=False)
    
    
    
