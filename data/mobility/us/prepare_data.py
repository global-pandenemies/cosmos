import pandas as pd
import numpy as np
import math
from datetime import datetime, timedelta

mobility_types = ['Workplace'] # types to consider

mobility_data=pd.read_csv('RAW/community_mobility_coleman.csv',dtype={'countyFIPS':str})
info_data=pd.read_csv('../../misc/us/info.csv',dtype={'UID':str})
info_data = info_data.set_index('UID')
hierarchy_data=pd.read_csv('../../misc/us/hierarchy.csv',dtype={'UID_parent':str,'UID_child':str})

final_cols = ['UID','location','mobility_type','date','mobility_change']
tmp_data = {'UID':[],'location':[],'mobility_type':[],'mobility_change':[],'date':[]}

# first filter and clean up existing nodes i.e. make sure that they have data for each day 
#(intermediate data will be trivially interpolated, earlier and later data trivially extrapolated)

mobility_data = mobility_data[mobility_data['mobility_type'].isin(mobility_types)].sort_values('date')
mobility_data['UID'] = ['840'+code.zfill(5) for code in mobility_data['countyFIPS']]

delta_1day = timedelta(days=1)
start_date = datetime.strptime(min(mobility_data['date']),"%Y-%m-%d")
end_date = datetime.strptime(max(mobility_data['date']),"%Y-%m-%d")

dates = []
while start_date <= end_date:
    dates.append(start_date.strftime("%Y-%m-%d"))
    start_date += delta_1day

data_new = {'UID':[], 'location':[], 'mobility_type':[], 'mobility_change':[], 'date':[]}

print(len(dates))

for mtype in mobility_types:

    mtype_data = mobility_data[mobility_data['mobility_type']==mtype]

    existing_nodes = set(mtype_data['UID'])

    for uid in existing_nodes:
        
        last_value = 0.0        
        i_date = 0

        data_new['date'] += dates
        data_new['UID'] += [uid]*len(dates)        
        data_new['location'] += [info_data.loc[uid,'location']]*len(dates)
        data_new['mobility_type'] += [mtype]*len(dates)

        for mob_type,row in mtype_data[mtype_data['UID']==uid].iterrows():
            
            while ((i_date < len(dates) and dates[i_date] != row['date'])):
                if dates[i_date] > row['date']:
                    print('ERROR! sorting wrong?')
                data_new['mobility_change'].append(last_value)             
                i_date+=1

            data_new['mobility_change'].append(row['mobility_change'])
            last_value = row['mobility_change']
            i_date+=1

        while (i_date < len(dates)):
            data_new['mobility_change'].append(last_value)             
            i_date+=1
        
# add new data
print(mobility_data.shape)
#print(data_new)
print(len(data_new['UID']),len(data_new['location']),len(data_new['mobility_type']),len(data_new['date']),len(data_new['mobility_change']))
#mobility_data = mobility_data.append(pd.DataFrame(data_new)).sort_values('date')
mobility_data = pd.DataFrame(data_new).sort_values('date')
  
# interpolate values for parents if not existing (use existing children and weight by population)

for mtype in mobility_types:

    mtype_data = mobility_data[mobility_data['mobility_type']==mtype]

    existing_nodes = set(mtype_data['UID'])    
    missing_nodes = (set(hierarchy_data[hierarchy_data['UID_child'].isin(existing_nodes)]['UID_parent']) - existing_nodes)

    # first collect all nodes that need to be constructed
    while True:
        
        delta_missing = (set(hierarchy_data[hierarchy_data['UID_child'].isin(missing_nodes)]['UID_parent']) - existing_nodes - missing_nodes)
        print(delta_missing)
        missing_nodes = missing_nodes.union(delta_missing)

        if(len(delta_missing)==0):
            break

        # now construct the nodes by interpolate from existing children (check if any children are on the missing list. In this case construct them first recursively)

    def construct_node(uid):

        global data_new, existing_nodes, mobility_data, mtype_data
    
        if uid in existing_nodes: #nothing to do
            #print ("Nothing to do:",uid)
            return

        children = set(hierarchy_data[hierarchy_data['UID_parent']==uid]['UID_child'])
        for child in children:
            construct_node(child)

        children = list(children.intersection(existing_nodes))
        if len(children)==0:
            return
        # use all existing children to construct parent                    

        parent_value = [0.0]*len(dates)
        total_mass = 0
        for child in children:
            #print(mtype_data[mtype_data['UID']==child]['mobility_change'])
            weight = info_data.loc[uid,'Population'] 
            #print(weight,parent_value)
            parent_value = list(map(sum, zip(parent_value,np.array(mtype_data[mtype_data['UID']==child]['mobility_change'])*weight)))
            total_mass += weight

        if (total_mass == 0):
            return
        parent_value = [i/total_mass for i in parent_value]

        print('constructing',uid)

        data_new = {}
        data_new['date'] = dates
        data_new['UID'] = [uid]*len(dates)
        data_new['location'] = [info_data.loc[uid,'location']]*len(dates)
        data_new['mobility_type'] = [mtype]*len(dates)
        data_new['mobility_change'] = parent_value

        mobility_data = mobility_data.append(pd.DataFrame(data_new))
        mtype_data = mobility_data[mobility_data['mobility_type']==mtype]

        existing_nodes.add(uid)            

    print(missing_nodes)
    for uid in list(missing_nodes):    
        construct_node(uid)

print(mobility_data.shape)
mobility_data.to_csv('community_mobility.csv',columns=final_cols, index=False)
    
print("DONE")
    
    
