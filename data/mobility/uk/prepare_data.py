import pandas as pd
import numpy as np
import math
import sys
from datetime import datetime, timedelta

loc_fixing_mobility = {
'Angus':'Angus council',
'Antrim and Newtownabbey':'Antrim And Newtownabbey',
'Ards and North Down':'Ards And North Down',
'Argyll and Bute':'Argyll and Bute Council',
'Armagh City and Banbridge and Craigavon':'Armagh City and Banbridge And Craigavon',
'Halton':'Borough of Halton',
'Bridgend':'Bridgend County Borough',
'Caerphilly':'Caerphilly County Borough',
'Bristol, City of':'City of Bristol',
'Conwy':'Conwy Principal Area',
'Cornwall and Isles of Scilly':'Cornwall',
'Derry and Strabane':'Derry And Strabane',
'Dundee City':'Dundee City Council',
'East Ayrshire':'East Ayrshire Council',
'East Dunbartonshire':'East Dunbartonshire Council',
'East Lothian':'East Lothian Council',
'East Renfrewshire':'East Renfrewshire Council',
'City of Edinburgh':'Edinburgh',
'Fermanagh and Omagh':'Fermanagh And Omagh',
'Herefordshire, County of':'Herefordshire',
'Highland':'Highland Council',
'Kingston upon Hull, City of':'Kingston upon Hull',
'Merthyr Tydfil':'Merthyr Tydfil County Borough',
'Mid and East Antrim':'Mid And East Antrim',
'Neath Port Talbot':'Neath Port Talbot Principle Area',
'Newry and Mourne and Down':'Newry and Mourne And Down',
'North Ayrshire':'North Ayrshire Council',
'Rhondda Cynon Taf':'Rhondda Cynon Taff',
'South Ayrshire':'South Ayrshire Council',
'Torfaen':'Torfaen Principal Area',
'West Dunbartonshire':'West Dunbartonshire Council',
'Wrexham':'Wrexham Principal Area',
}


mobility_types = ['Workplace'] # types to consider

mobility_data=pd.read_csv('RAW/community_mobility_change.csv')
mobility_data.drop_duplicates(subset=['location','parent_loc','mobility_type','date'],keep='first', inplace=True)

info_data=pd.read_csv('../../misc/uk/info.csv',dtype={'UID':str})
info_data = info_data.set_index('UID')
hierarchy_data=pd.read_csv('../../misc/uk/hierarchy.csv',dtype={'UID_parent':str,'UID_child':str})

location_lookup = {}
for index, row in hierarchy_data.iterrows():
    #print(index,row)
    name_child = row['name_child']
    name_parent = row['name_parent']
    if name_child in loc_fixing_mobility.keys():
        name_child = loc_fixing_mobility[name_child]
    if name_parent in loc_fixing_mobility.keys():
        name_parent = loc_fixing_mobility[name_parent]
    location_lookup[name_child] = row['UID_child']
    location_lookup[name_parent] = row['UID_parent']

#print (location_lookup)
print (mobility_data.columns)

final_cols = ['UID','location','mobility_type','date','mobility_change']
tmp_data = {'UID':[],'location':[],'mobility_type':[],'mobility_change':[],'date':[]}

# first filter and clean up existing nodes i.e. restrict to United Kingdom and  make sure that they have data for each day 
#(intermediate data will be trivially interpolated, earlier and later data trivially extrapolated)

mobility_data = mobility_data[mobility_data['mobility_type'].isin(mobility_types) & ((mobility_data['location'] == "United Kingdom") | (mobility_data['parent_loc'] == "United Kingdom"))].sort_values('date')

#now check if we can identify each location
locations = np.unique(mobility_data['location'])

n=0
for location in locations:    
    if location not in location_lookup.keys():
        print("ERROR: location not identifiable:",location)
        n +=1

if n>0:
    print('WARNING:',n,'out of',len(locations),'unidentified')

uids = []
for i, row in mobility_data.iterrows():
    uids.append(location_lookup[row['location']])
mobility_data['UID']=uids

#print(mobility_data)

delta_1day = timedelta(days=1)
start_date = datetime.strptime(min(mobility_data['date']),"%Y-%m-%d %H:%M:%S")
end_date = datetime.strptime(max(mobility_data['date']),"%Y-%m-%d %H:%M:%S")

dates = []
dates_short = []
while start_date <= end_date:
    dates.append(start_date.strftime("%Y-%m-%d %H:%M:%S"))
    dates_short.append(start_date.strftime("%Y-%m-%d"))
    start_date += delta_1day

data_new = {'UID':[], 'location':[], 'mobility_type':[], 'mobility_change':[], 'date':[]}

print(len(dates))

for mtype in mobility_types:

    mtype_data = mobility_data[mobility_data['mobility_type']==mtype]
    print(mtype_data)

    existing_nodes = set(mtype_data['UID'])

    for uid in existing_nodes:
        
        last_value = 0.0        
        i_date = 0

        data_new['date'] += dates_short
        data_new['UID'] += [uid]*len(dates)        
        data_new['location'] += [info_data.loc[uid,'location']]*len(dates)
        data_new['mobility_type'] += [mtype]*len(dates)

        #print(mtype_data[mtype_data['UID']==uid])

        for mob_type,row in mtype_data[mtype_data['UID']==uid].iterrows():
            
            #print(i_date,dates[i_date],row)

            while ((i_date < len(dates) and dates[i_date] != row['date'])):
                #print("===",i_date,dates[i_date])
                if dates[i_date] > row['date']:
                    print('ERROR! sorting wrong?')
                data_new['mobility_change'].append(last_value)             
                i_date+=1

            data_new['mobility_change'].append(row['mobility_change'])
            last_value = row['mobility_change']
            i_date+=1

        while (i_date < len(dates)):
            data_new['mobility_change'].append(last_value)             
            i_date+=1
        
# add new data
print(mobility_data.shape)
#print(data_new)
print(len(data_new['UID']),len(data_new['location']),len(data_new['mobility_type']),len(data_new['date']),len(data_new['mobility_change']))
mobility_data = pd.DataFrame(data_new).sort_values('date')
  
# interpolate values for parents if not existing (use existing children and weight by population)

for mtype in mobility_types:

    mtype_data = mobility_data[mobility_data['mobility_type']==mtype]

    existing_nodes = set(mtype_data['UID'])    
    missing_nodes = (set(hierarchy_data[hierarchy_data['UID_child'].isin(existing_nodes)]['UID_parent']) - existing_nodes)

    # first collect all nodes that need to be constructed
    while True:
        
        delta_missing = (set(hierarchy_data[hierarchy_data['UID_child'].isin(missing_nodes)]['UID_parent']) - existing_nodes - missing_nodes)
        print(delta_missing)
        missing_nodes = missing_nodes.union(delta_missing)

        if(len(delta_missing)==0):
            break

        # now construct the nodes by interpolate from existing children (check if any children are on the missing list. In this case construct them first recursively)

    def construct_node(uid):

        global data_new, existing_nodes, mobility_data, mtype_data
    
        print('processing',uid)

        children = set(hierarchy_data[hierarchy_data['UID_parent']==uid]['UID_child'])
        for child in children:
            construct_node(child)

        if (len(children.intersection(existing_nodes)))!= len(children) and len(children.intersection(existing_nodes))>0:
            print("WARNING:",uid,len(children.intersection(existing_nodes)),"/",len(children),"Missing children",children-children.intersection(existing_nodes))

        if uid in existing_nodes: #nothing to do
            #print ("Nothing to do:",uid)
            return

        children = list(children.intersection(existing_nodes))
        if len(children)==0:
            return
        # use all existing children to construct parent                    

        parent_value = [0.0]*len(dates)
        total_mass = 0
        for child in children:
            #print(mtype_data[mtype_data['UID']==child]['mobility_change'])
            weight = info_data.loc[uid,'Population'] 
            #print(weight,parent_value)
            parent_value = list(map(sum, zip(parent_value,np.array(mtype_data[mtype_data['UID']==child]['mobility_change'])*weight)))
            total_mass += weight

        if (total_mass == 0):
            return
        parent_value = [i/total_mass for i in parent_value]

        print('constructing',uid)

        data_new = {}
        data_new['date'] = dates_short
        data_new['UID'] = [uid]*len(dates)
        data_new['location'] = [info_data.loc[uid,'location']]*len(dates)
        data_new['mobility_type'] = [mtype]*len(dates)
        data_new['mobility_change'] = parent_value

        mobility_data = mobility_data.append(pd.DataFrame(data_new))
        mtype_data = mobility_data[mobility_data['mobility_type']==mtype]

        existing_nodes.add(uid)            

    print(missing_nodes)
    for uid in list(missing_nodes):    
        construct_node(uid)

duplicates = mobility_data[mobility_data.duplicated(['UID', 'date'], keep=False)]

print("DUPLICATES:",duplicates)              
                           
mobility_data.sort_values(['UID', 'date'], ascending=[1, 1], inplace=True)
print(mobility_data.shape)
mobility_data.to_csv('community_mobility.csv',columns=final_cols, index=False)
    
print("DONE")
    
    
